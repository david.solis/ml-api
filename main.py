from fastapi import FastAPI

personas = ['David', 'Roberto','Gaby']

app = FastAPI()

@app.get('/')
def index():
    return {'mensaje': 'Bienvenidos al App Diabetes'}


@app.get('/personas')
def get_personas():
    return personas

@app.get('/personas/{persona_id}')
def get_persona_by_id(persona_id: int):
    return {'persona': personas[persona_id]}
